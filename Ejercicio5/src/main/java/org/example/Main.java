package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        byte conteoA = 0;
        byte conteoE = 0;
        byte conteoI = 0;
        byte conteoO = 0;
        byte conteoU = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese una frase");
        String frase = scanner.nextLine();
        frase = frase.toUpperCase();
        byte longitudFrase = (byte) frase.length();
        for (int i = 0; i < frase.length(); i++) {
            char letraActual = frase.charAt(i);
            switch (letraActual){
                case 'A':
                    ++conteoA;
                    break;
                case 'E':
                    ++conteoE;
                    break;
                case 'I':
                    ++conteoI;
                    break;
                case 'O':
                    ++conteoO;
                    break;
                case 'U':
                    ++conteoU;
                    break;
            }
        }
        System.out.println("Cantidad de A = " + conteoA + "\n" +
                "Cantidad de E = " + conteoE + "\n" +
                "Cantidad de I = " + conteoI + "\n" +
                "Cantidad de O = " + conteoO + "\n" +
                "Cantidad de U = " + conteoU);
        System.out.println("Longitud de la frase con espacios " + longitudFrase);
    }
}




