package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Vamos a calcular el area de un circulo.\n" +
                "Por favor ingrese el radio");
        double radio = Double.parseDouble(scanner.nextLine());
        Double Area = Math.PI * Math.pow(radio, 2);
        System.out.println("Area = " + Area);
    }
}




