package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float numero = -1;
        while(numero < 0) {
            System.out.println("Ingrese un numero que sea mayor o igual a cero");
            numero = Float.parseFloat(scanner.nextLine());
        }
        System.out.println("Lo haz logrado, felicidades");
    }
}




