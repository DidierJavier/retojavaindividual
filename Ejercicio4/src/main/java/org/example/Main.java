package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese un dia de la semana para saber si es laborable");
        String diaSemana = scanner.nextLine();
        diaSemana = diaSemana.toUpperCase();
        switch (diaSemana) {
            case "LUNES" : case "MARTES" : case "MIERCOLES" : case "JUEVES" : case "VIERNES" :
                System.out.println("El dia " + diaSemana + " es laborable");
                break;
            case "SABADO" : case "DOMINGO" :
                System.out.println("El dia " + diaSemana + " no es laborable");
                break;
            default:
                System.out.println("El dia " + diaSemana + " no existe");
                break;
        }
    }
}




