package org.example;

import java.util.Scanner;

public class MenuPrincipal {
    public static void menuPrincipalUsuario(){
        var opcion = -1;
        var scanner = new Scanner(System.in);
        ICatalogoActores catalogo = new CatalogoActoresImplementacion();
            while (opcion != 0){
                System.out.println("****** GESTION DE ACTORES ********\n" +
                        "**Digite su opcion**\n" +
                        "1-INICIAR CATALOGO DE ACTORES\n" +
                        "2-AGREGAR ACTOR\n" +
                        "3-VER TODOS LOS ACTORES\n" +
                        "4-BUSCAR ACTOR\n" +
                        "0-SALIR.\n" +
                        "*********************************");
                opcion = Integer.parseInt(scanner.nextLine());

                switch (opcion) {
                    case 1:
                        catalogo.iniciarCatalogoActores();
                        break;
                    case 2:
                        System.out.println("Introduce el nombre del actor a agregar");
                        var nombrePelicula = scanner.nextLine();
                        catalogo.agregarActor(nombrePelicula);
                        break;
                    case 3:
                        catalogo.listarActores();
                        break;
                    case 4:
                        System.out.println("Introduce el nombre del actor a buscar");
                        var buscar = scanner.nextLine();
                        catalogo.buscarActor(buscar);
                        break;
                    case 0:
                        System.out.println("Hasta pronto");
                        break;
                    default:
                        System.out.println("Opción no reconocida");
                        break;
                }
            }

    }
}
