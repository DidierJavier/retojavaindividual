package org.example;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public interface IAccesoDatos {
    public boolean existe(String nombreRecurso) throws AccesoDatosExcepcion;

    public List<Actor> listar(String nombreRecurso) throws LecturaDatosExcepcion;

    public void escribir(Actor actor, String nombreRecurso, boolean anexar) throws EscrituraDatosExcepcion;

    public String buscar(String nombreRecurso, String buscar) throws LecturaDatosExcepcion;

    public void crear(String nombreRecurso) throws AccesoDatosExcepcion;

    public void borrar(String nombreRecurso) throws AccesoDatosExcepcion;
}
