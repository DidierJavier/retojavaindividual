package org.example;

public class AccesoDatosExcepcion  extends Exception{
    public AccesoDatosExcepcion(String mensaje) {
        super(mensaje);
    }
}
