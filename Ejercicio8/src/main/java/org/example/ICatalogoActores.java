package org.example;

public interface ICatalogoActores {
    String NOMBRE_RECURSO = "Actores.txt";

    public void agregarActor(String nombreActor);

    public void listarActores();

    public void buscarActor(String buscar);

    public void iniciarCatalogoActores();
}
