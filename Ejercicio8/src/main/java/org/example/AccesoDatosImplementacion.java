package org.example;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AccesoDatosImplementacion implements IAccesoDatos {

    @Override
    public boolean existe(String nombreRecurso) throws AccesoDatosExcepcion {
        var archivo = new File(nombreRecurso);//Se crea la variable archivo; no se crea un archivo
        return archivo.exists();//Se pregunta si el recurso existe; retorna verdadero o falso
    }

    @Override
    public List<Actor> listar(String nombreRecurso) throws LecturaDatosExcepcion {
        var archivo = new File(nombreRecurso);
        List<Actor> actores = new ArrayList<>();
        try {
            BufferedReader entrada = null;//Se lee la información del archivo
            try {
                entrada = new BufferedReader(new FileReader(archivo));
            } catch (FileNotFoundException ex) {
                throw new LecturaDatosExcepcion("Excepción al listar películas" + ex.getMessage());
            }
            String lectura = null;
            lectura = entrada.readLine();
            while(lectura != null){             //Ciclo while para leer la información hasta que haya una línea en blanco
                Actor actor = new Actor(lectura);//Por cada línea se crean objetos tipo película
                actores.add(actor);  //Se guardan los objetos tipo película en el Array
                lectura = entrada.readLine();
            }
            entrada.close();//Se cierra el flujo
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new LecturaDatosExcepcion("Excepción al listar películas" + ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new LecturaDatosExcepcion("Excepción al listar películas" + ex.getMessage());
        }
        return actores;
    }

    @Override
    public void escribir(Actor actor, String nombreRecurso, boolean anexar) throws EscrituraDatosExcepcion {
        File archivo = new File(nombreRecurso);
        try {
            PrintWriter salida = new PrintWriter(new FileWriter(archivo, anexar));//PrintWriter permite crear y escribir archivos
            salida.println(actor.toString());
            salida.close();
            System.out.println("Se ha escrito información al archivo" + actor);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new EscrituraDatosExcepcion("Excepción al escribir películas" + ex.getMessage());
        }
    }

    @Override
    public String buscar(String nombreRecurso, String buscar) throws LecturaDatosExcepcion {
        var archivo = new File(nombreRecurso);
        String resultado = null; //Se almacena el resultado de la búsqueda. Si se encuentra se retorna el nombre e índice de la película
        try {
            var entrada = new BufferedReader(new FileReader(archivo));
            String lectura = null;
            lectura = entrada.readLine();
            int indice = 1;
            while(lectura != null){
                if (buscar != null && buscar.equalsIgnoreCase(lectura)){
                    resultado = "Actor: " + lectura + ". Encontrado en el índice: " + indice + "\n";
                    break;
                }
                lectura = entrada.readLine();
                indice++;
            }
            entrada.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            throw new LecturaDatosExcepcion("Excepción al buscar película" + ex.getMessage());
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new LecturaDatosExcepcion("Excepción al buscar película" + ex.getMessage());
        }
        return resultado;
    }

    @Override
    public void crear(String nombreRecurso) throws AccesoDatosExcepcion {
        File archivo = new File(nombreRecurso);//Se crea un objeto de tipo file en memoria
        try {
            PrintWriter salida = new PrintWriter(new FileWriter(archivo));//PrintWriter permite crear y escribir archivos
            salida.close();//El archivo que se abrió se tiene que cerrar y en este momento se crea el archivo en memoria
            System.out.println("Se ha creado el archivo");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new AccesoDatosExcepcion("Excepción al crear archivo" + ex.getMessage());
        }
    }

    @Override
    public void borrar(String nombreRecurso) throws AccesoDatosExcepcion {
        var archivo = new File(nombreRecurso);
        if(archivo.exists())
            archivo.delete();
        System.out.println("Se ha borrado el archivo");
    }
}
