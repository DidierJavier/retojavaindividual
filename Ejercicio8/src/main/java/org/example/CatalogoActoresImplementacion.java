package org.example;

public class CatalogoActoresImplementacion implements ICatalogoActores{
    private final IAccesoDatos datos;

    public CatalogoActoresImplementacion() {
        this.datos = new AccesoDatosImplementacion();
    }

    @Override
    public void agregarActor(String nombrePelicula) {
        Actor pelicula = new Actor(nombrePelicula);
        boolean anexar = false;
        try {
            anexar = datos.existe(NOMBRE_RECURSO);
            datos.escribir(pelicula, NOMBRE_RECURSO, anexar);
        } catch (AccesoDatosExcepcion ex) {
            System.out.println("Error de acceso a datos");
            ex.printStackTrace(System.out);
        }
    }

    @Override
    public void listarActores() {
        try {
            var peliculas = this.datos.listar(NOMBRE_RECURSO);
            for (var pelicula : peliculas) {
                System.out.println("Actor = " + pelicula);
            }
        } catch (LecturaDatosExcepcion ex) {
            System.out.println("Error de acceso datos");
            ex.printStackTrace(System.out);
        }
    }

    @Override
    public void buscarActor(String buscar) {
        String resultado = null;
        try {
            resultado = this.datos.buscar(NOMBRE_RECURSO, buscar);
        } catch (LecturaDatosExcepcion ex) {
            System.out.println("Error de acceso datos");
            ex.printStackTrace(System.out);
        }
        System.out.println("resultado = " + resultado);
    }

    @Override
    public void iniciarCatalogoActores() {
        try {
            if(this.datos.existe(NOMBRE_RECURSO)){
                datos.borrar(NOMBRE_RECURSO);
                datos.crear(NOMBRE_RECURSO);
            }else{
                datos.crear(NOMBRE_RECURSO);
            }
        } catch (AccesoDatosExcepcion ex) {
            System.out.println("Error al iniciar catálogo de actores");
            ex.printStackTrace(System.out);
        }
    }
}
