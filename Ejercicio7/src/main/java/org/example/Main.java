package org.example;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Double> listaNumeros = new ArrayList<>();
        byte opcion = 1;
        do{
            System.out.println("Ingrese una lista de numeros para ordenar de forma descendente");
            System.out.println("Digite 1 para empezar a ingresar la lista de numeros");
            System.out.println("Digite 2 para finalizar");
            opcion = scanner.nextByte();
            if(opcion == 1){
                listaNumeros = ingresarNumeros(listaNumeros);
            }
            if(opcion == 2){
                ordenarNumeros(listaNumeros);
            }

        }while(opcion != 2);
    }

    private static ArrayList<Double> ingresarNumeros(ArrayList<Double> listaNumeros) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el numero");
        listaNumeros.add(scanner.nextDouble());
        return listaNumeros;
        }

    private static void ordenarNumeros(ArrayList<Double> listaNumeros) {
        Double nuevaLista[] = new Double[listaNumeros.size()];
        Double listaDescendente[] = new Double[nuevaLista.length];
        for (int i = 0; i < listaNumeros.size(); i++) {
            nuevaLista[i] = listaNumeros.get(i);
        }
        Arrays.sort(nuevaLista);

        System.out.println("La lista ordenada en forma descendente es:");
        for (int i = 0; i < nuevaLista.length; i++) {
            System.out.print(nuevaLista[i] + "; ");
        }
        System.out.println();
        int j = 0;
        for (int i = nuevaLista.length - 1; i >=0 ; i--) {
            listaDescendente[i] = nuevaLista[j];
            ++j;
        }
        for (int i = 0; i < listaDescendente.length; i++) {
            System.out.print(listaDescendente[i] + "; ");
        }
    }
}
    
    






