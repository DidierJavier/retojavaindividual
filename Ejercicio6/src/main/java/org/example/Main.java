package org.example;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Double> listaNumeros = new ArrayList<>();
        byte opcion = 1;
        do{
            System.out.println("Ingrese una lista de numeros para ordenar de forma ascendente");
            System.out.println("Digite 1 para empezar a ingresar la lista de numeros");
            System.out.println("Digite 2 para finalizar");
            opcion = scanner.nextByte();
            if(opcion == 1){
                listaNumeros = ingresarNumeros(listaNumeros);
                //System.out.println("listaNumeros = " + listaNumeros);
            }
            if(opcion == 2){
                ordenarNumeros(listaNumeros);
            }

        }while(opcion != 2);
    }

    private static ArrayList<Double> ingresarNumeros(ArrayList<Double> listaNumeros) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el numero");
        listaNumeros.add(scanner.nextDouble());
        return listaNumeros;
        }

    private static void ordenarNumeros(ArrayList<Double> listaNumeros) {
        Double nuevaLista[] = new Double[listaNumeros.size()];
        for (int i = 0; i < listaNumeros.size(); i++) {
            nuevaLista[i] = listaNumeros.get(i);
        }
        Arrays.sort(nuevaLista);
        System.out.println("La lista ordenada en forma ascendente es:");
        for (int i = 0; i < nuevaLista.length; i++) {
            System.out.print(nuevaLista[i] + "; ");
        }
    }
}
    
    






